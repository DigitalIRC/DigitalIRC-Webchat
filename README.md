DigitalIRC-Webchat
==================

Install
-------
Installation instructions are on the website:
http://qwebirc.org/faq

If you'd like to make modifications you'd find it a LOT
easier if you symlink:

js -> static/js/debug
css -> static/css/debug

... then you can browse to http://instance/quidebug.html
and use your favourite javascript debugger, as well as
not having to compile each time you make a change!

RECAPTCHA
---------

To turn reCAPTCHA on/off without restarting qwebirc, go
to qwebirc/engines/ajaxengine.py and you'll see
something like:

231      raise AJAXException, 'incorrect-captcha-sol'
232
233    postdict = dict(privatekey=config.RECAPTCHA_KEY[1],
234                    remoteip=request.getClientIP(),
235                    challenge=recaptchaChallenge,
236                    response=recaptchaResponse)

On line 233 where it says config.RECAPTCHA_KEY[1], replace
it with "privkey", which is your reCAPTCHA private key.
You'll still need to specify the tuple in your config,
but this will ensure it doesn't need to read the config
for it irregardless of the setting.

