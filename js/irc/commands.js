qwebirc.irc.Commands = new Class({
  Extends: qwebirc.irc.BaseCommandParser,
  initialize: function(parentObject) {
    this.parent(parentObject);
    
    this.aliases = {
      "J": "JOIN",
      "K": "KICK",
      "MSG": "PRIVMSG",
      "Q": "QUERY",
      "BACK": "AWAY",
      "HOP": "CYCLE",
      "PORN":"JOIN #empornium"
    };
  },
  
  newUIWindow: function(property) {
    var p = this.parentObject.ui[property];
    if(!$defined(p)) {
      this.getActiveWindow().errorMessage("Current UI does not support that command.");
    } else {
      p.bind(this.parentObject.ui)();
    }
  },
  
  /* [require_active_window, splitintoXargs, minargs, function] */
  cmd_ME: [true, undefined, undefined, function(args) {
    if(args == undefined)
      args = "";

    var target = this.getActiveWindow().name;
    if(!this.send("PRIVMSG " + target + " :\x01ACTION " + args + "\x01"))
      return;

    this.newQueryLine(target, "ACTION", args, {"@": this.parentObject.getNickStatus(target, this.parentObject.nickname)});
  }],
  cmd_CTCP: [false, 3, 2, function(args) {
    var target = args[0];
    var type = args[1].toUpperCase();
    var message = args[2];
    
    if(message == undefined)
      message = "";

    if(message == "") {
      if(!this.send("PRIVMSG " + target + " :\x01" + type + "\x01"))
        return;
    } else {
      if(!this.send("PRIVMSG " + target + " :\x01" + type + " " + message + "\x01"))
        return;
    }
  
    this.newTargetLine(target, "CTCP", message, {"x": type});
  }],
  cmd_PRIVMSG: [false, 2, 2, function(args) {
    var target = args[0];
    var message = args[1];
    
    if(!this.parentObject.isChannel(target))
      this.parentObject.pushLastNick(target);
    if(this.send("PRIVMSG " + target + " :" + message))
      this.newQueryLine(target, "MSG", message, {"@": this.parentObject.getNickStatus(target, this.parentObject.nickname)});  
  }],
  cmd_NOTICE: [false, 2, 2, function(args) {
    var target = args[0];
    var message = args[1];

    if(this.send("NOTICE " + target + " :" + message)) {
      if(this.parentObject.isChannel(target)) {
        this.newTargetLine(target, "NOTICE", message, {"@": this.parentObject.getNickStatus(target, this.parentObject.nickname)});
      } else {
        this.newTargetLine(target, "NOTICE", message);
      }
    }
  }],
  cmd_QUERY: [false, 2, 1, function(args) {
    if(this.parentObject.isChannel(args[0])) {
      this.getActiveWindow().errorMessage("Can't target a channel with this command.");
      return;
    }

    this.parentObject.newWindow(args[0], qwebirc.ui.WINDOW_QUERY, true);

    if((args.length > 1) && (args[1] != ""))
      return ["SAY", args[1]];
  }],
  cmd_SAY: [true, undefined, undefined, function(args) {
    if(args == undefined)
      args = "";
      
    return ["PRIVMSG", this.getActiveWindow().name + " " + args]
  }],
  cmd_LOGOUT: [false, undefined, undefined, function(args) {
    this.parentObject.ui.logout();
  }],
  cmd_OPTIONS: [false, undefined, undefined, function(args) {
    this.newUIWindow("optionsWindow");
  }],
  cmd_EMBED: [false, undefined, undefined, function(args) {
    this.newUIWindow("embeddedWindow");
  }],
  cmd_REGISTER: [false, undefined, undefined, function(args) {
    var self = this;
    new MooDialog.Prompt('Please enter a valid email address.', function(email) {
      if((!email.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i))) {
        new MooDialog.Alert('Please provide a valid email address.');
        MooDialog.Prompt('',email());
      } else {
        new MooDialog.Prompt('Please enter a password for this nick.', function(passwords) {
          if((passwords == "") || (passwords.length < 5) || (passwords.match(/\s/))) {
            new MooDialog.Alert('The password must be at least 5 characters and contain no spaces.');
            MooDialog.Prompt('',passwords());
          } else {
            self.send("PRIVMSG NickServ :REGISTER " + passwords + " " + email);
          }
        });          
      }
    });
  }],
  cmd_ABOUT: [false, undefined, undefined, function(args) {
    this.newUIWindow("aboutWindow");
  }],
  cmd_QUOTE: [false, 1, 1, function(args) {
    this.send(args[0]);
  }],
  cmd_COLOR: [false, 2, 2, function(args) {
    code = args[0];
    switch(code) {
      case '0': var name = "\u00030"; break;
      case '1': var name = "\u00031"; break;
      case '2': var name = "\u00032"; break;
      case '3': var name = "\u00033"; break;
      case '4': var name = "\u00034"; break;
      case '5': var name = "\u00035"; break;  
      case '6': var name = "\u00036"; break;
      case '7': var name = "\u00037"; break;
      case '8': var name = "\u00038"; break;
      case '9': var name = "\u00039"; break;
      case '10': var name = "\u000310"; break;
      case '11': var name = "\u000311"; break;
      case '12': var name = "\u000312"; break;
      case '13': var name = "\u000313"; break;
      case '14': var name = "\u000314"; break;
      case '15': var name = "\u000315"; break;
      case 'b':
      case 'B': var name = "\u0002"; break;
      case 'u': 
      case 'U': var name = "\u001F"; break;
      case 'i':
      case 'I': var name = "\u001D"; break;
      default: var name = "\u000F"; break;
    }
    if(name == undefined)
      name = "\u000F";
    return ["PRIVMSG", this.getActiveWindow().name + " " + unescape(encodeURIComponent(name)) + args[1]]
  }],
  cmd_KICK: [true, 2, 1, function(args) {
    var channel = this.getActiveWindow().name;
    
    var message = "";
    var target = args[0];
    
    if(args.length == 2)
      message = args[1];
    
    this.send("KICK " + channel + " " + target + " :" + message);
  }],
  automode: function(direction, mode, args) {
    var channel = this.getActiveWindow().name;

    var modes = direction;
    for(var i=0;i<args.length;i++)
      modes = modes + mode;
      
    this.send("MODE " + channel + " " + modes + " " + args.join(" "));
  },
  cmd_OP: [true, 6, 1, function(args) {
    this.automode("+", "o", args);
  }],
  cmd_DEOP: [true, 6, 1, function(args) {
    this.automode("-", "o", args);
  }],
  cmd_HALFOP: [true, 6, 1, function(args) {
    this.automode("+", "h", args);
  }],
  cmd_DEHALFOP: [true, 6, 1, function(args) {
    this.automode("-", "h", args);
  }],
  cmd_VOICE: [true, 6, 1, function(args) {
    this.automode("+", "v", args);
  }],
  cmd_DEVOICE: [true, 6, 1, function(args) {
    this.automode("-", "v", args);
  }],
  cmd_TOPIC: [true, 1, 1, function(args) {
    this.send("TOPIC " + this.getActiveWindow().name + " :" + args[0]);
  }],
  cmd_AWAY: [false, 1, 0, function(args) {
    this.send("AWAY :" + (args?args[0]:""));
  }],
  cmd_QUIT: [false, 1, 0, function(args) {
    this.send("QUIT :" + (args?args[0]:""));
  }],
  cmd_CYCLE: [true, 1, 0, function(args) {
    var c = this.getActiveWindow().name;
    
    this.send("PART " + c + " :" + (args?args[0]:"cycling"));
    this.send("JOIN " + c);
  }],
  cmd_JOIN: [false, 2, 1, function(args) {
    var channels = args.shift();
    
    var schans = channels.split(",");
    var fchans = [];
    
    var warn = false;
    
    schans.forEach(function(x) {
      if(x == "0")
        // /join 0 hack, supported in rizon's ircd
        this.getActiveWindow().infoMessage("Parting all channels...");
      else if(!this.parentObject.isChannel(x)) {
        x = "#" + x;
        warn = true;
      }
      fchans.push(x);
    }.bind(this));

    if(warn) {
      var delayinfo = function() {
        this.getActiveWindow().infoMessage("Channel names begin with # (corrected automatically).");
      }.bind(this).delay(250);
    }
      
    this.send("JOIN " + fchans.join(",") + " " + args.join(" "));
  }],
  cmd_UMODE: [false, 1, 0, function(args) {
    this.send("MODE " + this.parentObject.getNickname() + (args?(" " + args[0]):""));
  }],
  cmd_BEEP: [false, undefined, undefined, function(args) {
    this.parentObject.ui.beep();
  }],
  cmd_AUTOJOIN: [false, undefined, undefined, function(args) {
    return ["JOIN", this.parentObject.options.autojoin];
  }],
  cmd_CLEAR: [false, undefined, undefined, function(args) {
    var w = this.getActiveWindow().lines;
    while(w.childNodes.length > 0)
      w.removeChild(w.firstChild);
  }],
  cmd_PART: [false, 2, 0, function(args) {
    var w = this.getActiveWindow();
    var message = "";
    var channel;
    
    if(w.type != qwebirc.ui.WINDOW_CHANNEL) {
      if(!args || args.length == 0) {
        w.errorMessage("Insufficient arguments for command.");
        return;
      }
      channel = args[0];  
      if(args.length > 1)
        message = args[1];
    } else {
      if(!args || args.length == 0) {
        channel = w.name;
      } else {
        var isChan = this.parentObject.isChannel(args[0]);
        if(isChan) {
          channel = args[0];
          if(args.length > 1)
            message = args[1];
        } else {
          channel = w.name;
          message = args.join(" ");
        }
      }
    }
    
    this.send("PART " + channel + " :" + message);
  }]
});
